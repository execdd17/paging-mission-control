## Runtime
I found this particular challenge interesting, and wondered if I could get it to work at scale. This project was created to run on [Apache Spark](https://spark.apache.org/). The easiest way to run it is on the [Databricks](https://databricks.com/try-databricks) platform. The `.dbc` file can be [directly imported](https://docs.databricks.com/notebooks/notebooks-manage.html#import-a-notebook) into a Databricks workspace. From there you can run any of the three notebooks. I will also include individual notebook exports (as .scala files) if you want to look at the code in your IDE. The notebooks can also be imported individually into Databricks.

### Notebooks
*Mission Control Runner* - The entry point notebook that would typically contain the `main` method in non-notebook environments.

*Mission Control Tests* - The integration tests that would normally go in `src/test/scala`

*Mission Control* - The Class and Companion Object that defines the business logic, typically in `src/main/scala` 

#### Notebook Input
Notebooks that take input have "widgets". The only input is the location of the raw data. Within the integration tests I create two sample data sets. One was provided with the challenge, and the other is derivative.

### Production Readiness
For production, these notebooks would likely be (among other things):
1. Converted into a gradle/sbt project 
2. Turned into an Uber jar to grab any dependencies that weren't already on the Spark classpath
3. Submitted as a jar and run on a dedicated compute cluster