// Databricks notebook source
// MAGIC %run "./Mission Control"

// COMMAND ----------

val sampleData = """20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT
20180101 23:01:09.521|1000|17|15|9|8|7.8|BATT
20180101 23:01:26.011|1001|101|98|25|20|99.8|TSTAT
20180101 23:01:38.001|1000|101|98|25|20|102.9|TSTAT
20180101 23:01:49.021|1000|101|98|25|20|87.9|TSTAT
20180101 23:02:09.014|1001|101|98|25|20|89.3|TSTAT
20180101 23:02:10.021|1001|101|98|25|20|89.4|TSTAT
20180101 23:02:11.302|1000|17|15|9|8|7.7|BATT
20180101 23:03:03.008|1000|101|98|25|20|102.7|TSTAT
20180101 23:03:05.009|1000|101|98|25|20|101.2|TSTAT
20180101 23:04:06.017|1001|101|98|25|20|89.9|TSTAT
20180101 23:04:11.531|1000|17|15|9|8|7.9|BATT
20180101 23:05:05.021|1001|101|98|25|20|89.9|TSTAT
20180101 23:05:07.421|1001|17|15|9|8|7.9|BATT
"""
  
val sampleDataPath = "dbfs:/tmp/mission-control-test/sample.csv"
dbutils.fs.put(sampleDataPath, sampleData, overwrite=true)

// COMMAND ----------

val missionControl = MissionControl(filePath = sampleDataPath)
val alerts = missionControl.findAlerts

val expectedRows = Array(
  Row("1000", "RED LOW", "BATT", "2018-01-01T23:01:09.521Z"),
  Row("1000", "RED HIGH", "TSTAT", "2018-01-01T23:01:38.001Z")
)

val actualRows = alerts.orderBy($"component").collect()
assert(actualRows.length == expectedRows.length, "The actual row counts do not match!")

for (i <- 0 to 1)
    assert(actualRows(i) == expectedRows(i), "The row values do not match!")

// COMMAND ----------

// MAGIC %md
// MAGIC Test with a lot more data (more than 7M). The vast majority are dupes, but we are pretending for the test that they legitimate readings coming back at a very high rate. Another approach would be to generate data that spanned more time, but this was easier, and it's all just an exercise. It can take a few minutes to generate the data!

// COMMAND ----------

// NOTE: this string grows at 2^N where N is the number of iterations. 
// 19 iterations is over 500,000 copies of the original sample data
val bigDupe = (1 until 19).foldLeft(new StringBuilder(sampleData)) { (duplicatedData, i) =>
  duplicatedData ++= duplicatedData
}
val sampleDataWithDupesPath = "dbfs:/tmp/mission-control-test/sample-with-dupes.csv"
dbutils.fs.put(sampleDataWithDupesPath, bigDupe.toString, overwrite=true)

// COMMAND ----------

// MAGIC %md
// MAGIC With our new data we get one more expected alert

// COMMAND ----------

val missionControl = MissionControl(filePath = "dbfs:/tmp/mission-control-test/sample-with-dupes.csv")
val alerts = missionControl.findAlerts

val expectedRows = Array(
  Row("1000", "RED LOW", "BATT", "2018-01-01T23:02:11.302Z"),
  Row("1000", "RED HIGH", "TSTAT", "2018-01-01T23:01:38.001Z"),
  Row("1001", "RED LOW", "BATT", "2018-01-01T23:05:07.421Z")
)

val actualRows = alerts.orderBy($"satellite-id", $"component").collect()
assert(actualRows.length == expectedRows.length, "The actual row counts do not match!")

for (i <- 0 to 1)
    assert(actualRows(i) == expectedRows(i), "The row values do not match!")
