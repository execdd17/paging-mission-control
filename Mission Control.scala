// Databricks notebook source
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import org.apache.spark.sql.Column

object MissionControl {
   private val schemaDDL: String = """
    timestamp TIMESTAMP,
    `satellite-id` STRING,
    `red-high-limit` SHORT,
    `yellow-high-limit` SHORT,
    `yellow-low-limit` SHORT,
    `red-low-limit` SHORT,
    `raw-value` FLOAT, 
    component STRING """
  
  def apply(filePath: String): MissionControl = {
     val rawTelemetry = spark.read
      .format("csv")
      .option("sep", "|")
      .option("timestampFormat", "yyyyMMdd HH:mm:ss.SSS")
      .schema(schemaDDL)
      .load(filePath)
    
    new MissionControl(rawTelemetry)
  }
}

final class MissionControl(val rawTelemetry: DataFrame) {
  /**
    * This method attempts to find satellite alerts based on the specification here: 
    * https://gitlab.com/enlighten-challenge/paging-mission-control.
    * @param rawTelemetry The DataFrame created from reading a raw '|' delimited telemetry file.
    *                     The schemaDDL definition above must be used
    * @return A DataFrame containing all alerts found
    * NOTE: This approach WILL NOT ALWAYS find the earliest violation timestamp within a window,
    * but it will still meet the requirements as written. It was unlear if the earliest violation timestamp was required.
    * Performance will be better with this approach, but it can be changed if that actually is a requirement.
    */
  def findAlerts: DataFrame = {
    // Depending on the component, we want ranks to reflect higher or lower raw values
    val highRanks = Window.partitionBy(col("satellite-id"), col("component"), col("window.start"))
      .orderBy(col("raw-value").desc)
    val lowRanks = Window.partitionBy(col("satellite-id"), col("component"), col("window.start"))
      .orderBy(col("raw-value").asc)

    val temperatureComp: Column = col("component") === "TSTAT"

    val redZone = rawTelemetry
      .withColumn("inRedZone",
        when(temperatureComp, col("raw-value") > col("red-high-limit"))
        .otherwise(col("raw-value") < col("red-low-limit")))
      .filter(col("inRedZone")) // this will reduce the dataset size prior to creating a shuffle stage in spark

    // We use row_number instead of rank because it's theoretically possible for raw-values to be identical
    val ranked = redZone
      .withColumn("window", window(col("timestamp"), "5 minutes"))
      .withColumn("rank",
        when(temperatureComp, row_number.over(highRanks))
        .otherwise(row_number.over(lowRanks)))

    val severity: Column = when(temperatureComp, lit("RED HIGH"))
      .otherwise(lit("RED LOW"))
      .alias("severity")
    
    val earliestTimestamp: Column = array_sort(col("timestamps"))(0)
    val formattedTimestamp: Column = date_format(earliestTimestamp, "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
      .alias("timestamp")

    val alerts = ranked
      .filter(col("rank") <= 3) // all records at this point are red zone and we only need three to trigger an alert
      .groupBy(col("satellite-id"), col("component"), col("window.start"))
      .agg(
        collect_list(col("timestamp")).as("timestamps"),
        count("*").as("numRedZones")) // collect_list memory growth is limited to at most 3 timestamps
      .filter(col("numRedZones") === 3) 
      .select(col("satellite-id"), severity, col("component"), formattedTimestamp)

    alerts
  }
}

