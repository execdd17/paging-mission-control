// Databricks notebook source

// COMMAND ----------

dbutils.widgets.text("fileLocation", "")
var fileLocation = dbutils.widgets.get("fileLocation")

// COMMAND ----------

// MAGIC %run "./Mission Control"

// COMMAND ----------

val missionControl = MissionControl(filePath=fileLocation)
val alerts = missionControl.findAlerts
display(alerts)

// COMMAND ----------

val alertsLocal = alerts.select(to_json(struct("*")).as("json")).collect()
val jsonFormattedAlerts = alertsLocal.map(_.getAs[String]("json")).mkString("[", ",", "]")

// COMMAND ----------

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.module.scala.DefaultScalaModule

val mapper = new ObjectMapper()
mapper.registerModule(DefaultScalaModule)
mapper.enable(SerializationFeature.INDENT_OUTPUT)

val deserializedResult = mapper.readValue(jsonFormattedAlerts, classOf[Array[Map[String,Any]]])
println(mapper.writeValueAsString(deserializedResult))
